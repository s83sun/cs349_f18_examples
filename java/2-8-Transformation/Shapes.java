/*
* CS 349 Java Code Examples
* Graphics Transformations
*
* Move shapes around the screen
*/
import javax.swing.JFrame;
import javax.swing.JComponent;
import java.awt.*;
import java.awt.geom.*;
import java.lang.Math.*;

public class Shapes {
	
    public static void main(String[] args) {
    	Canvas canvas = new Canvas();
        JFrame f = new JFrame("Shape Transformations"); 
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(500, 520);
        f.setContentPane(canvas);
        f.setBackground(Color.WHITE);     
        f.setVisible(true);
	}
}

class Canvas extends JComponent {

	int width = 500;
	int height = 500;
	int block = 50;
	AffineTransform initialMatrix = null;
	Graphics2D g2 = null;

	public void paintComponent(Graphics g) {
		super.paintComponent(g); 
		g2 = (Graphics2D)g;
		initialMatrix = g2.getTransform();

		// make a centred grid
		drawGrid();

		// reset();
		// translate(-1,-1);
		// scale(1,2);
		// translate(6,4);
		// shapeA();

		// reset();

		// translate(-4,-3);
		// scale(1,2);
		// translate(4,4);
		// shapeB();

		translate(2,0);
		rotate(90);
		shapeA();

		// show model shapes
		// reset();	// reset for each set, not needed on midterm
		// shapeA();
		// shapeB();

		// (b)
		// reset();	// reset for each set, not needed on midterm
		// translate(8,0);
		// shapeA();

		// reset();
		// translate(-3,6);
		// shapeB();

		// (c)
		// reset(); 	// reset for each set, not needed on midterm
		// translate(5,2);
		// scale(1,2);
		// shapeA();

		// reset();
		// translate(3, 2);
		// scale(1,2);
		// translate(-3, -2);
		// shapeB();

		// (d) 
		// reset(); // reset for each set, not needed on midterm
		// translate(2,0);
		// rotate(90);
		// shapeA();

		// reset();
		// translate(5,2);
		// rotate(90);
		// translate(-3,-2);
		// shapeB();

	} 

	// model for shape A
	// positioned at 0,0, and 2x2 squares
	private void shapeA() {
		g2.setStroke(new BasicStroke(2));
		g2.setColor(Color.BLACK.brighter());
		g2.setFont(new Font("SansSerif", Font.PLAIN, 18));
		drawBlock(0, 0, 2, 2, "A");
	}

	// model for shape B
	// positioned at 3,2 and 2x2 squares
	private void shapeB() {
		g2.setStroke(new BasicStroke(2));
		g2.setColor(Color.BLACK.brighter());
		g2.setFont(new Font("SansSerif", Font.PLAIN, 18));
		drawBlock(3, 2, 2, 2, "B");
	}

	// reset affine transformation matrix
	private void reset() {
		g2.setColor(Color.BLACK);
		g2.setTransform(initialMatrix);
	}

	// rotate matrix by theta degrees
	private void rotate(double theta) {
		g2.rotate(Math.toRadians(theta));
	}

	// scale matrix by sx and sy
	private void scale(double sx, double sy) {
		g2.scale(sx, sy);
	}

	// translate by tx and ty
	private void translate(double tx, double ty) {
		g2.translate(tx * block, ty * block);
	}

	// draw blocks
	private void drawBlock(int x, int y, int w, int h, String text) {
		g2.drawRect(x * block, y * block, w * block, h * block);
		g2.drawString(text, (x * block) + (w * block / 2) - 5, (y * block) + (h * block / 2) + 5);
	}

	// draw grid lines
	private void drawGrid() {
		g2.setStroke(new BasicStroke(1));
		g2.setColor(Color.GRAY.brighter());

		// horizontal lines
		for(int i = 0; i <= height; i += block) {
			if (i == (5 * block)) { 
				g2.setStroke(new BasicStroke(5));
			} else {
				g2.setStroke(new BasicStroke(1));
			}
			g2.drawLine(0, i, width, i);
		}
		// vertical lines
		for(int i = 0; i <= width; i += block) {
			if (i == (5 * block)) {
				g2.setStroke(new BasicStroke(5));
			} else {
				g2.setStroke(new BasicStroke(1));
			}			
			g2.drawLine(i, 0, i, height);
		}
	}
}
