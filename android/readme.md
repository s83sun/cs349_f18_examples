## Android Demos

> These demos include projects that need to be opened in [Android Studio](http://developer.android.com) or [IntelliJ 2018.1 Community Edition](https://www.jetbrains.com/idea/download).